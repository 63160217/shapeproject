/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.shapeproject;

/**
 *
 * @author User
 */
public class Triangle {
    private double b ;
    private double h ;
    public Triangle (double b,double h){
        this.b = b;
        this.h = h;
    }
    public double calArea(){
        return 1/2.0 * b * h;
    }
    public double getB(){
        return b;
    }
    public double getH(){
        return h;
    }
    public void setB(double b){
        if (b <= 0){
            System.out.println("Error : Base must more than zero");
            return;
        }
        this.b = b;
    }
    public void setH(double h){
        if (h <= 0){
            System.out.println("Error : Height must more than zero");
            return;
        }
        this.h = h;
    }
}
